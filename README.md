# Installation

1. Edit composer.json:

```json
{
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@gitlab.com:Paroleen/payments.git"
        }
    ],
    "require": {
        "parol/payments": "^1.0"
    }
}
```
2. Install the library:

```bash
composer install
```
