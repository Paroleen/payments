<?php
namespace Payments;

require_once(__DIR__ . '/../../../../vendor/autoload.php');

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__. '/../../../../');
$dotenv->load();
$dotenv->required(['DB_HOST', 'DB_PORT', 'DB_DATABASE', 'DB_USERNAME', 'DB_PASSWORD', 'CARTS_TABLE', 'COURSES_TABLE', 'ORDERS_TABLE', 'USERS_TABLE', 'STRIPE_PUBLIC_KEY', 'STRIPE_PRIVATE_KEY']);

class Stripe {
    private \Stripe\StripeClient $stripe;
    private \mysqli $mysqli;
    private $user;

    /**
     * @param int User ID
     */
    public function __construct(int $user_id) {
        $this->stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);
        $this->mysqli = new \mysqli($_ENV['DB_HOST'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD'], $_ENV['DB_DATABASE'], $_ENV['DB_PORT']);
        $this->user_id = $user_id;

        if ($this->mysqli->connect_error)
            die("Connection failed: " . $mysqli->connect_error);

        $this->user = $this->getUser($user_id);
    }

    /**
     * Get user data from DB
     *
     * @return object User data
     */
    private function getUser() {
        $result = $this->mysqli->query('SELECT * FROM ' . $_ENV['USERS_TABLE'] . ' WHERE id = ' . $this->user_id);
        return $result->fetch_object();
    }

    /**
     * Save stripe Customer ID inside the user table
     *
     * @param $customer_id Stripe Customer ID
     */
    private function updateCustomerId(string $customer_id) {
        $stmt = $this->mysqli->prepare('UPDATE ' . $_ENV['USERS_TABLE'] . ' SET customer_id = ? WHERE id = ?');
        $stmt->bind_param('si', $customer_id, $this->user->id);
        $stmt->execute();

        $this->user = $this->getUser($this->user->id);
    }

    /**
     * Create and save a Stripe Customer
     */
    public function createCustomer() {
        if (!isset($this->user->customer_id)) {
            $customer = $this->stripe->customers->create([
                'name' => $this->user->name . ' ' . $this->user->surname,
                'email' => $this->user->email
            ]);
            $this->updateCustomerId($customer->id);
        }
    }

    /**
     * Update customer informations
     *
     * @param $params Customer data
     */
    public function updateCustomer(array $params) {
        return $this->stripe->customers->update($this->user->customer_id, $params);
    }

    /**
     * Update Customer email info (changes email address only on the Stripe customer not the DB)
     *
     * @param $email Email address
     * @return object Stripe Response
     */
    public function updateEmail(string $email) {
        return $this->updateCustomer([ 'email' => $email ]);
    }

    /**
     * Return cart data from DB
     *
     * @return array Array containing 'cart' elements and the price 'total'
     */
    public function getCart() {
        $timestamp = date('Y-m-d H:i:s');
        $cart = $this->mysqli->query('SELECT course_id AS id, title, description, thumbnail, price FROM ' . $_ENV['CARTS_TABLE'] . ' INNER JOIN ' . $_ENV['COURSES_TABLE'] . ' ON ' . $_ENV['CARTS_TABLE'] . '.course_id = ' . $_ENV['COURSES_TABLE'] . '.id WHERE `user_id` = ' . $this->user->id . ' AND `timestamp` <= "' . $timestamp . '"');

        if ($cart->num_rows > 0)
            $total = $this->mysqli->query('SELECT SUM(price) AS total FROM ' . $_ENV['CARTS_TABLE'] . ' INNER JOIN ' . $_ENV['COURSES_TABLE'] . ' ON ' . $_ENV['CARTS_TABLE'] . '.course_id = ' . $_ENV['COURSES_TABLE'] . '.id WHERE `user_id` = ' . $this->user->id . ' AND `timestamp` <= "' . $timestamp . '"')->fetch_object()->total;

        return [ 'cart' => $cart, 'total' => $total ];
    }

    /**
     * Prepare the payment by retriving data from the cart and creating a Stripe PaymentIntent
     *
     * @param string $card_id Card ID
     * @param string $currency Currency (default: 'eur')
     * @return string Client Secret for Stripe.js
     */
    public function preparePayment(string $card_id = null, string $currency = 'eur') {
        $timestamp = date('Y-m-d H:i:s');
        $cart = $this->mysqli->query('SELECT course_id AS id, title, description, thumbnail, price FROM ' . $_ENV['CARTS_TABLE'] . ' INNER JOIN ' . $_ENV['COURSES_TABLE'] . ' ON ' . $_ENV['CARTS_TABLE'] . '.course_id = ' . $_ENV['COURSES_TABLE'] . '.id WHERE `user_id` = ' . $this->user->id . ' AND `timestamp` <= "' . $timestamp . '"');

        if ($cart->num_rows > 0) {
            $total = $this->mysqli->query('SELECT SUM(price) AS total FROM ' . $_ENV['CARTS_TABLE'] . ' INNER JOIN courses ON ' . $_ENV['CARTS_TABLE'] . '.course_id = ' . $_ENV['COURSES_TABLE'] . '.id WHERE `user_id` = ' . $this->user->id . ' AND `timestamp` <= "' . $timestamp . '"')->fetch_object()->total;

            $attributes = [
                'amount' => $total,
                'currency' => $currency,
                'customer' => $this->user->customer_id,
                'metadata' => [
                    'user_id' => $this->user->id,
                    'order_timestamp' => $timestamp
                ]
            ];
            if (isset($card_id))
                $attributes['payment_method'] = $card_id;
            $intent = $this->stripe->paymentIntents->create($attributes);

            return [ 'cart' => $cart, 'total' => $total, 'client_secret' => $intent->client_secret ];
        }
    }

    /**
     * Prepare the payment by retriving data from the cart and creating a Stripe PaymentIntent.
     * It uses the default card associated with the Customer
     *
     * @param string $price_id Stripe Price ID associated with a subscription
     * @return string Client Secret for Stripe.js
     */
    public function prepareSubscription(string $price_id) {
        $attributes = [
            'customer' => $this->user->customer_id,
            'items' => [[
                'price' => $price_id,
            ]],
            'metadata' => [
                'user_id' => $this->user->id
            ],
            'payment_behavior' => 'default_incomplete',
            'expand' => ['latest_invoice.payment_intent'],
        ];
        $subscription = $this->stripe->subscriptions->create($attributes);

        return [ 'subscriptionId' => $subscription->id, 'clientSecret' => $subscription->latest_invoice->payment_intent->client_secret ];
    }

    /**
     * Cancel subscription
     */
    public function removeSubscription() {
        $subscription = $this->mysqli->query('SELECT stripe_id AS id FROM ' . $_ENV['ORDERS_TABLE'] . ' WHERE `user_id` = ' . $this->user->id . ' AND subscription = 1');
        if ($subscription->num_rows > 0)
            return $this->stripe->subscriptions->cancel($subscription->fetch_object()->id, []);
    }

    /**
     * Prepare new card insertion
     *
     * @return string Client Secret for Stripe.js
     */
    public function prepareCard() {
        $intent = $this->stripe->setupIntents->create([
            'customer' => $this->user->customer_id,
            'metadata' => [
                'user_id' => $this->user->id
            ]
        ]);
        return $intent->client_secret;
    }

    /**
     * Updates default card
     *
     * @param string $card_id Stripe Card ID
     * @return object Stripe response
     */
    public function updateCard($card_id) {
        return $this->updateCustomer([ 'invoice_settings' => [
            'default_payment_method' => $card_id
        ]]);
    }

    /**
     * Return the Customer's default card, otherwise search for the first card saved by a certain Customer and set it as default payment
     *
     * @return object Stripe Card object
     */
    public function getCard() {
        if (isset($this->user->customer_id))
            $card = $this->stripe->customers->retrieve($this->user->customer_id, [])->invoice_settings->default_payment_method;

        if (!isset($card)) {
            $cards = $this->stripe->paymentMethods->all([
                'customer' => $this->user->customer_id,
                'type' => 'card',
            ]);
            if (isset($cards) && count($cards) > 0) {
                $this->updateCard($cards->data[0]->id);
                $card = $cards->data[0];
            }
        } else
            $card = $this->stripe->paymentMethods->retrieve($card, []);
        return $card;
    }

    /**
     * Remove Customer's default card
     *
     * @return object Stripe response
     */
    public function removeCard() {
        if (isset($this->user->customer_id))
            $card = $this->stripe->customers->retrieve($this->user->customer_id, [])->invoice_settings->default_payment_method;

        if (isset($card))
            return $this->stripe->paymentMethods->detach($card, []);
    }

    /**
     * Create a subscription plan
     *
     * @param string $name Name
     * @param string $description Description
     * @param int $amount Amount
     * @param string $currency Default: 'eur'
     * @param bool $active Default: true
     * @param string $period Default: 'month'
     * @return object Stripe response
     */
    public static function createPlan(string $name, string $description, int $amount, string $currency = 'eur', bool $active = true, string $period = 'month') {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        $price = $stripe->prices->create([
            'unit_amount' => $amount,
            'currency' => $currency,
            'recurring' => ['interval' => $period],
            'product_data' => [
                'name' => $name,
                'active' => $active
            ],
        ]);

        if (isset($description)) {
            $stripe->products->update($price->product, [
                'description' => $description
            ]);
        }

        return $price;
    }

    /**
     * Remove a subscription plan
     * WARNING: it removes a plan only if there's no associated price
     *
     * @param string $product_id Stripe Product ID
     * @return object Stripe response
     */
    public static function removePlan(string $product_id) {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        return $stripe->products->delete($product_id, []);
    }

    /**
     * Return a subscription plan
     *
     * @param string $price_id Price ID
     * @return object Stripe Price object
     */
    public static function getPlan($price_id) {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        return $stripe->prices->retrieve($price_id, [ 'expand' => ['product'] ]);
    }

    /**
     * Return all the subscription plans
     *
     * @param bool $active default: true
     * @return object Stripe Prices object
     */
    public static function getPlans(bool $active = true) {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        return $stripe->prices->all([ 'type' => 'recurring', 'active' => $active, 'expand' => ['data.product'] ])->data;
    }

    /**
     * Return a payment (PaymentIntent)
     *
     * @param string $payment_id PaymentIntent ID
     * @return object Stripe PaymentIntent object
     */
    public static function getPayment($payment_id) {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        return $stripe->paymentIntents->retrieve($payment_id, []);
    }

    /**
     * Return a subscription
     *
     * @param string $subscription_id Subscription ID
     * @return object Stripe Prices object
     */
    public static function getSubscription($subscription_id) {
        $stripe = new \Stripe\StripeClient($_ENV['STRIPE_PRIVATE_KEY']);

        return $stripe->subscriptions->retrieve($subscription_id);
    }
}
