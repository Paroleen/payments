<?php
    namespace Payments;

    require_once(__DIR__ . '/../../../../vendor/autoload.php');
    include_once('WebhookInterface.php');
    include_once('Stripe.php');

    class StripeWebhook {
        /**
         * Create a webhook page
         *
         * @param WebhookInterface $callbacks Additional responses
         * @param bool $default Use default responses
         */
        public static function create(WebhookInterface $callbacks = null, $default = true) {
            $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__. '/../../../../');
            $dotenv->load();
            $dotenv->required(['DB_HOST', 'DB_PORT', 'DB_DATABASE', 'DB_USERNAME', 'DB_PASSWORD', 'CARTS_TABLE', 'COURSES_TABLE', 'ORDERS_TABLE', 'USERS_TABLE' , 'STRIPE_WEBHOOK_KEY']);

            $mysqli = new \mysqli($_ENV['DB_HOST'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD'], $_ENV['DB_DATABASE'], $_ENV['DB_PORT']);
            $endpoint_secret = $_ENV['STRIPE_WEBHOOK_KEY'];

            $payload = @file_get_contents('php://input');
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
            $event = null;

            try {
                $event = \Stripe\Webhook::constructEvent(
                    $payload, $sig_header, $endpoint_secret
                );
            } catch(\UnexpectedValueException $e) {
                // Invalid payload
                http_response_code(400);
                exit();
            } catch(\Stripe\Exception\SignatureVerificationException $e) {
                // Invalid signature
                http_response_code(400);
                exit();
            }

            // Handle the event
            if ($default) {
                switch ($event->type) {
                    case 'payment_intent.succeeded':
                        $paymentIntent = $event->data->object;

                        if (isset($paymentIntent->metadata->order_timestamp)) {
                            $stmt = $mysqli->prepare('SELECT course_id AS id FROM ' . $_ENV['CARTS_TABLE'] . ' WHERE `user_id` = ? AND `timestamp` <= ?');
                            $stmt->bind_param('is', $paymentIntent->metadata->user_id, $paymentIntent->metadata->order_timestamp);
                            $stmt->execute();
                            $courses = $stmt->get_result();
                    
                            $stmt = $mysqli->prepare('INSERT INTO ' . $_ENV['ORDERS_TABLE'] . ' (`user_id`, course_id, subscription, `status`, stripe_id, `timestamp`) VALUES (?, ?, 0, "succeeded", ?, ?)');

                            $mysqli->query('START TRANSACTION');
                            while ($course = $courses->fetch_array()) {
                                $stmt->bind_param('isss', $paymentIntent->metadata->user_id, $course['id'], $paymentIntent->id, $paymentIntent->metadata->order_timestamp);
                                $stmt->execute();
                            }
                            $stmt->close();
                            $mysqli->query('COMMIT');

                            $stmt = $mysqli->prepare('DELETE FROM ' . $_ENV['CARTS_TABLE'] . ' WHERE `user_id` = ? AND `timestamp` <= ?');
                            $stmt->bind_param('is', $paymentIntent->metadata->user_id, $paymentIntent->metadata->order_timestamp);
                            $stmt->execute();
                        }
                        break;
                    case 'customer.subscription.created':
                        $subscription = $event->data->object;

                        $timestamp = date('Y-m-d H:i:s', $subscription->start_date);
                        $stmt = $mysqli->prepare('INSERT INTO ' . $_ENV['ORDERS_TABLE'] . ' (`user_id`, course_id, subscription, `status`, stripe_id, `timestamp`) VALUES (?, NULL, 1, ?, ?, ?)');
                        $stmt->bind_param('isss', $subscription->metadata->user_id, $subscription->status, $subscription->id, $timestamp);
                        $stmt->execute();

                        break;
                    case 'customer.subscription.updated':
                        $subscription = $event->data->object;

                        $stmt = $mysqli->prepare('UPDATE ' . $_ENV['ORDERS_TABLE'] . ' SET `status` = ? WHERE `user_id` = ? AND `stripe_id` = ?;');
                        $stmt->bind_param('sis', $subscription->status, $subscription->metadata->user_id, $subscription->id);
                        $stmt->execute();

                        break;
                    case 'customer.subscription.deleted':
                        $subscription = $event->data->object;

                        $stmt = $mysqli->prepare('DELETE FROM ' . $_ENV['ORDERS_TABLE'] . ' WHERE `stripe_id` = ?');
                        $stmt->bind_param('s', $subscription->id);
                        $stmt->execute();

                        break;
                    default:
                        echo 'Received unknown event type ' . $event->type;
                }
            }

            // Custom callbacks
            if (isset($callbacks))
                $callbacks->execute($event);

            http_response_code(200);
        }
    }
?>