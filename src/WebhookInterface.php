<?php
    namespace Stripe;

    interface WebhookInterface {
        public function execute($event);
    }
?>